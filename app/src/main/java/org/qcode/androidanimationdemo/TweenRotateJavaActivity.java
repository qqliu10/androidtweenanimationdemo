package org.qcode.androidanimationdemo;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.TextView;

public class TweenRotateJavaActivity extends Activity {

    private View mStartAnimation;
    private View mStopAnimation;
    private TextView mTxtViewFrameAnimationContainer;
    private RotateAnimation mRotateAnimation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_rotate_animation);

        mTxtViewFrameAnimationContainer = (TextView) findViewById(R.id.textview_frame_animation_container);
        mStartAnimation = findViewById(R.id.btn_start_animation);
        mStopAnimation = findViewById(R.id.btn_stop_animation);

        mStartAnimation.setOnClickListener(mOnClickListener);
        mStopAnimation.setOnClickListener(mOnClickListener);

        mRotateAnimation = new RotateAnimation(-180, -90,
                Animation.RELATIVE_TO_SELF, 0.1f,
                Animation.RELATIVE_TO_SELF, 0.1f);
        mRotateAnimation.setFillAfter(true);
        mRotateAnimation.setInterpolator(new AccelerateDecelerateInterpolator());
        mRotateAnimation.setDuration(1000);
    }

    private View.OnClickListener mOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.btn_start_animation:
                    mTxtViewFrameAnimationContainer.startAnimation(mRotateAnimation);
                    break;
                case R.id.btn_stop_animation:
                    mTxtViewFrameAnimationContainer.clearAnimation();
                    break;
            }
        }
    };
}
