package org.qcode.androidanimationdemo;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.animation.TranslateAnimation;
import android.widget.TextView;

public class TweenTranslateActivity extends Activity {

    private View mStartAnimation;
    private View mStopAnimation;
    private TextView mTxtViewFrameAnimationContainer;
    private TranslateAnimation mTranslateAnimation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_translate_animation);

        mTxtViewFrameAnimationContainer = (TextView) findViewById(R.id.textview_frame_animation_container);
        mStartAnimation = findViewById(R.id.btn_start_animation);
        mStopAnimation = findViewById(R.id.btn_stop_animation);

        mStartAnimation.setOnClickListener(mOnClickListener);
        mStopAnimation.setOnClickListener(mOnClickListener);

        mTranslateAnimation = (TranslateAnimation) AnimationUtils.loadAnimation(this, R.anim.tween_translate_animation_demo);
    }

    private View.OnClickListener mOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.btn_start_animation:
                    mTxtViewFrameAnimationContainer.startAnimation(mTranslateAnimation);
                    break;
                case R.id.btn_stop_animation:
                    mTxtViewFrameAnimationContainer.clearAnimation();
                    break;
            }
        }
    };
}
