package org.qcode.androidanimationdemo;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.TextView;

public class TweenTranslateJavaActivity extends Activity {

    private View mStartAnimation;
    private View mStopAnimation;
    private TextView mTxtViewFrameAnimationContainer;
    private TranslateAnimation mTranslateAnimation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_translate_animation);

        mTxtViewFrameAnimationContainer = (TextView) findViewById(R.id.textview_frame_animation_container);
        mStartAnimation = findViewById(R.id.btn_start_animation);
        mStopAnimation = findViewById(R.id.btn_stop_animation);

        mStartAnimation.setOnClickListener(mOnClickListener);
        mStopAnimation.setOnClickListener(mOnClickListener);

        mTranslateAnimation = new TranslateAnimation(TranslateAnimation.RELATIVE_TO_SELF, 0.5f,
                TranslateAnimation.RELATIVE_TO_SELF, 1f,
                TranslateAnimation.RELATIVE_TO_SELF, 0.5f,
                TranslateAnimation.RELATIVE_TO_SELF, 1f);
        mTranslateAnimation.setFillAfter(true);
        mTranslateAnimation.setInterpolator(new AccelerateDecelerateInterpolator());
        mTranslateAnimation.setDuration(1000);
        mTranslateAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                //动画开始执行时回调
                Log.d("TEST11", "onAnimationStart");
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                //动画结束执行时回调
                Log.d("TEST11", "onAnimationEnd");
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
                //动画重复执行时回调
                Log.d("TEST11", "onAnimationRepeat");
            }
        });
//        mTranslateAnimation.setRepeatCount(10);
//        mTranslateAnimation.setBackgroundColor(Color.RED);
//        mTranslateAnimation.setDetachWallpaper(true);
//        mTranslateAnimation.setFillEnabled(false);
//        mTranslateAnimation.setFillBefore(true);
//        mTranslateAnimation.setRepeatMode(Animation.REVERSE);
//        mTranslateAnimation.setStartOffset(500);
//        mTranslateAnimation.setStartTime(System.currentTimeMillis() + 1000);
//        mTranslateAnimation.setZAdjustment(Animation.ZORDER_TOP);
    }

    private View.OnClickListener mOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.btn_start_animation:
                    mTxtViewFrameAnimationContainer.startAnimation(mTranslateAnimation);
                    break;
                case R.id.btn_stop_animation:
                    mTxtViewFrameAnimationContainer.clearAnimation();
                    break;
            }
        }
    };
}
