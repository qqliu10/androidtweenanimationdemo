package org.qcode.androidanimationdemo;

import android.app.Activity;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class FrameAnimationActivity extends Activity {

    private View mStartAnimation;
    private View mStopAnimation;
    private TextView mTxtViewFrameAnimationContainer;
    private ImageView mImgViewFrameAnimationContainer;
    private AnimationDrawable mAnimationDrawableBg;
    private AnimationDrawable mAnimationDrawableSrc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_animation_set);

        mTxtViewFrameAnimationContainer = (TextView) findViewById(R.id.textview_frame_animation_container);
        mImgViewFrameAnimationContainer = (ImageView) findViewById(R.id.imgview_frame_animation_container);
        mStartAnimation = findViewById(R.id.btn_start_animation);
        mStopAnimation = findViewById(R.id.btn_stop_animation);

        mStartAnimation.setOnClickListener(mOnClickListener);
        mStopAnimation.setOnClickListener(mOnClickListener);

        mAnimationDrawableBg = new AnimationDrawable();
        mAnimationDrawableBg.addFrame(getResources().getDrawable(R.color.color_red), 200);
        mAnimationDrawableBg.addFrame(getResources().getDrawable(R.color.color_green), 200);
        mAnimationDrawableBg.addFrame(getResources().getDrawable(R.color.color_blue), 200);
        mAnimationDrawableBg.setOneShot(false);

        mAnimationDrawableSrc = new AnimationDrawable();
        mAnimationDrawableSrc.addFrame(getResources().getDrawable(R.color.color_red), 200);
        mAnimationDrawableSrc.addFrame(getResources().getDrawable(R.color.color_green), 200);
        mAnimationDrawableSrc.addFrame(getResources().getDrawable(R.color.color_blue), 200);
        mAnimationDrawableSrc.setOneShot(false);
    }

    private View.OnClickListener mOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            AnimationDrawable mAnimationDrawableBg =
                    (AnimationDrawable) mTxtViewFrameAnimationContainer.getBackground();
            AnimationDrawable mAnimationDrawableSrc =
                    (AnimationDrawable) mImgViewFrameAnimationContainer.getDrawable();

            switch (v.getId()) {
                case R.id.btn_start_animation:
//                    mImgViewFrameAnimationContainer.setImageDrawable(mAnimationDrawableBg);
//                    mTxtViewFrameAnimationContainer.setBackgroundDrawable(mAnimationDrawableBg);
                    if(!mAnimationDrawableBg.isRunning()) {
                        mAnimationDrawableBg.start();
                    }

                    if(!mAnimationDrawableSrc.isRunning()) {
                        mAnimationDrawableSrc.start();
                    }
                    break;
                case R.id.btn_stop_animation:
                    if(mAnimationDrawableBg.isRunning()) {
                        mAnimationDrawableBg.setVisible(false, false);
                    }

                    if(mAnimationDrawableSrc.isRunning()) {
                        mAnimationDrawableSrc.setVisible(false, false);
                    }
                    break;
            }
        }
    };
}
