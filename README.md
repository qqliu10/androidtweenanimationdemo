# AndroidTweenAnimationDemo
An simple frame animation & tween amination(view animation) demo in android
This project demonstrate how to use tween animation (including alpha/translate/scale/rotate animation).

FrameAnimationActivity: demonstrate how to user frame animation;

TweenAlphaActivity: demonstrate how to use alpha animation by xml;

TweenAlphaJavaActivity: demonstrate how to use alpha animation by java code;

TweenRotateActivity: demonstrate how to use rotate animation by xml;

TweenRotateJavaActivity: demonstrate how to use rotate animation by java code;

TweenScaleActivity: demonstrate how to use scale animation by xml;

TweenScaleJavaActivity: demonstrate how to use scale animation by java code;

TweenTranslateActivity: demonstrate how to use translate animation by xml;

TweenTranslateJavaActivity: demonstrate how to use translate animation by java code;

TweenAnimationSetActivity: demonstrate how to use tween animation set by xml;

TweenAnimationSetJavaActivity: demonstrate how to use tween animation set by java code;